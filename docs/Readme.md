# Poker Application Instructions

This application implements the specification in the **docs/poker-exercise.pdf**.

## Prerequisites

To build and run the application you will need to have both **Java** and **Apache Ant** pre-installed and in your program path.  You can get **Apache Ant** from https://ant.apache.org.  This application has been successfully built and tested with Java 16, but might also work with other Java versions of 8 and above.

## Build

Simply open a terminal or shell, go to the folder containing the **build.xml** file, and type:

```
ant
```

This should generate a **poker.jar** file in the same folder.

## Run

Type:

```
java -jar poker.jar <input file>
```

 where **\<input file\>** is the input data file, and has the same format as the sample input files in the **test** folder.  The format is described in more detail in the **docs/poker-exercise.pdf**.

For example:

```
java -jar poker.jar test/small-sample.txt
```

This should give the following output:

> Player 1 : 3 hands
> Player 2 : 2 hands

## Further Options

Running with the **-v** (verbose) or **-V** (very verbose) options will yield more detailed, line-by-line output.  For example:

```
java -jar poker.jar -v test/small-sample.txt
```

> Line 1: Player 2 Wins
> Line 2: Player 1 Wins
> Line 3: Player 2 Wins
> Line 4: Player 1 Wins
> Line 5: Player 1 Wins
> Player 1 : 3 hands
> Player 2 : 2 hands

or:

```
java -jar poker.jar -V test/small-sample.txt
```

> Line 1: "4H 4C 6S 7S KD 2C 3S 9S 9D TD" => Player 2 Wins
> 	 Player 1 [1 Pair: King of Diamonds, 7 of Spaces, 6 of Spaces, 4 of Hearts, 4 of Clubs]
> 	 Player 2 [1 Pair: 10 of Diamonds, 9 of Spaces, 9 of Diamonds, 3 of Spaces, 2 of Clubs]
> Line 2: "5D 8C 9S JS AC 2C 5C 7D 8S QH" => Player 1 Wins
> 	 Player 1 [High Card: Ace of Clubs, Jack of Spaces, 9 of Spaces, 8 of Clubs, 5 of Diamonds]
> 	 Player 2 [High Card: Queen of Hearts, 8 of Spaces, 7 of Diamonds, 5 of Clubs, 2 of Clubs]
> Line 3: "2D 9C AS AH AC 3D 6D 7D TD QD" => Player 2 Wins
> 	 Player 1 [3 of a Kind: Ace of Spaces, Ace of Hearts, Ace of Clubs, 9 of Clubs, 2 of Diamonds]
> 	 Player 2 [Flush: Queen of Diamonds, 10 of Diamonds, 7 of Diamonds, 6 of Diamonds, 3 of Diamonds]
> Line 4: "4D 6S 9H QH QC 3D 6D 7H QD QS" => Player 1 Wins
> 	 Player 1 [1 Pair: Queen of Hearts, Queen of Clubs, 9 of Hearts, 6 of Spaces, 4 of Diamonds]
> 	 Player 2 [1 Pair: Queen of Diamonds, Queen of Spaces, 7 of Hearts, 6 of Diamonds, 3 of Diamonds]
> Line 5: "2H 2D 4C 4D 4S 3C 3D 3S 9S 9D" => Player 1 Wins
> 	 Player 1 [Full House: 4 of Clubs, 4 of Diamonds, 4 of Spaces, 2 of Hearts, 2 of Diamonds]
> 	 Player 2 [Full House: 9 of Spaces, 9 of Diamonds, 3 of Clubs, 3 of Diamonds, 3 of Spaces]
> Player 1: 3 hands
> Player 2: 2 hands

These options are mostly intended for troubleshooting the application or the input data.

## Ties

In most cases, as described in the **docs/poker-exercise.pdf**,  the result of each game (i.e. input line) will be a win to either **Player 1** or **Player 2**.  However, there might also be occasional ties.  For example, if both players have a royal flush, but in different suits.  The original problem definition precludes resolution via card suit.  Such an instance occurs in the **test/extra-sample.txt** input:

```
java -jar poker.jar -V test/extra-sample.txt
```

> Line 1: "TD JD QD KD AD TH JH QH KH AH" => Tied
> 	 Player 1 [Royal Flush: Ace of Diamonds, King of Diamonds, Queen of Diamonds, Jack of Diamonds, 10 of Diamonds]
> 	 Player 2 [Royal Flush: Ace of Hearts, King of Hearts, Queen of Hearts, Jack of Hearts, 10 of Hearts]
> Line 2: "TD JD QD KD AD TH JH QH KH AD" => Player 1 Wins
> 	 Player 1 [Royal Flush: Ace of Diamonds, King of Diamonds, Queen of Diamonds, Jack of Diamonds, 10 of Diamonds]
> 	 Player 2 [Straight: Ace of Diamonds, King of Hearts, Queen of Hearts, Jack of Hearts, 10 of Hearts]
> Line 3 "TD JD QD KD AD TH JH QH KH" ERROR: 9 cards in line, should be 10
> Line 4 "TD JD QD KD AD TH JH QH KH AH 7D" ERROR: 11 cards in line, should be 10
> Line 5 "TD JD QD KD AD TH JH XH KH AH" ERROR: Illegal rank for card code 'XH'
> Line 6 "TD JD QD KD AD TH JH QY KH AH" ERROR: Illegal suit for card code 'QY'
> Player 1: 1 hands
> Player 2: 0 hands
> Ties: 1 hands
> Errors: 4 hands

This sample also contains some erroneous inputs. 