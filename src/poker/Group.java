package poker;

/**
 * A group of cards of the same rank.
 */
public class Group implements Comparable<Group>
{
    /**
     * The rank of the cards in the group.
     */
    private final Rank rank;

    /**
     * The number of cards in the group.  This should be 2, 3 or 4.
     */
    private final int size;

    /**
     * Creates a card group.
     * 
     * @param rank
     *            the rank of the cards in the group.
     * 
     * @param size
     *            the number of cards in the group.
     */
    public Group(Rank rank, int size)
    {
        this.rank = rank;
        this.size = size;
    }

    /**
     * Returns the card rank.
     * 
     * @return the rank.
     */
    public Rank getRank()
    {
        return this.rank;
    }

    /**
     * Returns the size of the group.
     * 
     * @return the size of the group.
     */
    public int getSize()
    {
        return this.size;
    }

    /**
     * Compares this group with another. Returns -1, 0, or 1 if this group is less than, equal to, or higher than the second.
     * 
     * @param other
     *            the other group.
     */
    @Override
    public int compareTo(Group other)
    {
        if (other == null)
        {
            return 1;
        }
        else
        {
            int cmp = Integer.compare(this.size, other.size);

            if (cmp == 0)
            {
                return this.rank.compareTo(other.rank);
            }
            else
            {
                return cmp;
            }
        }
    }
}
