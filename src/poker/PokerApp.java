package poker;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * The poker application, including the main program.
 */
public class PokerApp implements Runnable
{
    /**
     * The verbosity of the output.
     */
    private final Verbosity verbosity;

    /**
     * The input file.
     */
    private final Path inputPath;

    /**
     * Line index, where the first line is index 1. This will also be the number of lines read to date.
     */
    private int lineIndex = 0;

    /**
     * Map to track the results.
     */
    private final Map<Result, AtomicInteger> resultsMap = new HashMap<>();

    /**
     * The output print stream.
     */
    private final static PrintStream out = GameConstants.OUTPUT_STREAM;

    /**
     * Creates an instance of the poker application.
     * 
     * @param inputPath
     *            the file's input path. This must be non-null.
     * 
     * @param verbosity
     *            the verbosity.
     */
    private PokerApp(Path inputPath, Verbosity verbosity)
    {
        this.inputPath = inputPath;

        if (verbosity == null)
        {
            this.verbosity = Verbosity.NONE;
        }
        else
        {
            this.verbosity = verbosity;
        }

        // Initialise the map.
        for (Result result : Result.values())
        {
            this.resultsMap.put(result, new AtomicInteger(0));
        }
    }

    /**
     * Processes a single line from the input stream.
     * 
     * @param line
     *            the text line.
     */
    private void processLine(String line)
    {
        this.lineIndex++;
        Game game = Game.createGame(line);
        
        if (game == null)
        {
            // Probably an empty line.
            return;
        }
        
        Result result = game.getResult();

        AtomicInteger count = this.resultsMap.get(result);
        count.incrementAndGet();

        // Output data for this line, if verbosity turned on.
        printLineData(this.lineIndex, game, line);
    }

    /**
     * Prints data for a specific line, if verbosity switched on.
     * 
     * @param lineIndex
     *            the line index.
     * 
     * @param game
     *            the game details.
     *            
     *            @param lineInput the actual input line
     */
    private void printLineData(int lineIndex, Game game, String lineInput)
    {
        if (this.verbosity == Verbosity.NONE)
        {
            return;
        }

        Result result = game.getResult();

        if (result == Result.ERROR)
        {
            String errorMessage = MessageFormat.format("Line {0} \"{1}\" ERROR: {2}", this.lineIndex, lineInput, game.getErrorMessage());
            out.println(errorMessage);
            return;
        }

        String outcome = "";

        switch (result)
        {
            case TIE:
            {
                outcome = "Tied";
                break;
            }

            case WIN:
            {
                outcome = MessageFormat.format("{0} Wins", GameConstants.PLAYER_1_NAME);
                break;
            }

            case LOSS:
            {
                outcome = MessageFormat.format("{0} Wins", GameConstants.PLAYER_2_NAME);
                break;
            }

            default:
                // Already handled.
        }

        Hand hand1 = game.getHand1();
        Hand hand2 = game.getHand2();

        switch (this.verbosity)
        {
            case HIGH:
            {
                String message = MessageFormat.format("Line {0}: \"{1}\" => {2}\n\t {3} [{4}]\n\t {5} [{6}]",
                                                      this.lineIndex,
                                                      lineInput,
                                                      outcome,
                                                      GameConstants.PLAYER_1_NAME,
                                                      hand1,
                                                      GameConstants.PLAYER_2_NAME,
                                                      hand2);

                out.println(message);
                break;
            }

            case LOW:
            {

                String message = MessageFormat.format("Line {0}: {1}", this.lineIndex, outcome);

                out.println(message);
                break;
            }

            default:
        }
    }

    /**
     * Displays the summary.
     */
    private void printSummary()
    {
        final String template = "{0}: {1} hands";
        
        int wins = this.resultsMap.get(Result.WIN).get();
        String player1 = MessageFormat.format(template, GameConstants.PLAYER_1_NAME, wins);
        out.println(player1);
        
        int loss = this.resultsMap.get(Result.LOSS).get();
        String player2 = MessageFormat.format(template, GameConstants.PLAYER_2_NAME, loss);
        out.println(player2);
        
        int ties = this.resultsMap.get(Result.TIE).get();
        
        if (ties > 0)
        {
            String tie = MessageFormat.format(template, "Ties", ties);
            out.println(tie);
        }
        
        int errors = this.resultsMap.get(Result.ERROR).get();
        
        if (errors > 0)
        {
            String error = MessageFormat.format(template, "Errors", errors);
            out.println(error);
        }
    }

    /**
     * Executes the application.
     */
    @Override
    public void run()
    {
        try
        {
            Files.lines(this.inputPath).filter(Objects::nonNull).map(p -> p.trim()).filter(p -> !p.isEmpty()).forEach(line -> processLine(line));
        }
        catch (IOException ex)
        {
            throw new PokerException("IO Error at line {0}, application terminated", this.lineIndex);
        }

        // Print the results summary
        printSummary();
    }

    /**
     * The main program.
     * 
     * @param args
     *            the command line arguments.
     */
    public static void main(String[] args)
    {
        try
        {
            List<String> arguments = Arrays.asList(args)
                                           .stream()
                                           .filter(Objects::nonNull)
                                           .map(p -> p.trim())
                                           .filter(p -> !p.isEmpty())
                                           .collect(Collectors.toList());

            Verbosity verbose = null;
            Path filePath = null;

            for (String arg : arguments)
            {
                if (arg.equals(GameConstants.HIGH_VERBOSITY_OPTION))
                {
                    if (verbose == null)
                    {
                        verbose = Verbosity.HIGH;
                    }
                    else
                    {
                        throw new PokerException("Duplicate argument for verbosity");
                    }
                }
                else if (arg.equals(GameConstants.LOW_VERBOSITY_OPTION))
                {
                    if (verbose == null)
                    {
                        verbose = Verbosity.LOW;
                    }
                    else
                    {
                        throw new PokerException("Duplicate argument for verbosity");
                    }
                }
                else if (filePath == null)
                {
                    filePath = Paths.get(arg);

                    if (!Files.exists(filePath))
                    {
                        throw new PokerException("File ''{0}'' does not exist", filePath);
                    }
                    else if (!Files.isRegularFile(filePath))
                    {
                        throw new PokerException("File ''{0}'' is not a regular file", filePath);
                    }
                }
                else
                {
                    throw new PokerException("Duplicate argument for file path");
                }
            }

            if (verbose == null)
            {
                verbose = Verbosity.NONE;
            }

            if (filePath == null)
            {
                throw new PokerException("Must specify input file");
            }

            PokerApp poker = new PokerApp(filePath, verbose);
            poker.run();
        }
        catch (PokerException ex)
        {
            out.println(ex.getMessage());
        }
        catch (Throwable ex)
        {
            ex.printStackTrace(out);
        }
    }

}
