package poker;

/**
 * Enumeration for the type of a hand, in order of value.
 */
public enum HandType
{
    HIGH_CARD("High Card"),
    PAIR("1 Pair"),
    TWO_PAIRS("2 Pairs"),
    THREE_OF_A_KIND("3 of a Kind"),
    STRAIGHT("Straight"),
    FLUSH("Flush"),
    FULL_HOUSE("Full House"),
    FOUR_OF_A_KIND("4 of a Kind"),
    STRAIGHT_FLUSH("Straight Flush"),
    ROYAL_FLUSH("Royal Flush");

    /**
     * The printable name of this type. This will be non-null.
     */
    private final String name;

    /**
     * Creates an enumerator for a type of hand.
     * 
     * @param name
     *            the printable name. This must be non-null.
     */
    private HandType(String name)
    {
        if (name == null)
        {
            throw new PokerException("Null name");
        }

        this.name = name;
    }

    /**
     * Returns the printable name. This will be non-null.
     * 
     * @return the printable name.
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Compares 2 types and returns -1, 0 or 1 if the first type is less than, equal, or greater than the second.
     * 
     * @param type1
     *            the first type
     * 
     * @param type2
     *            the second type
     * 
     * @return -1, 0 or 1
     */
    public static int compare(HandType type1, HandType type2)
    {
        if (type1 == type2)
        {
            return 0;
        }
        else if (type1 == null)
        {
            return -1;
        }
        else if (type2 == null)
        {
            return 1;
        }
        else
        {
            return Integer.compare(type1.ordinal(), type2.ordinal());
        }
    }
}
