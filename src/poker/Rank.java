package poker;

/**
 * The rank of a card.
 */
public enum Rank
{
    CARD_2('2'),
    CARD_3('3'),
    CARD_4('4'),
    CARD_5('5'),
    CARD_6('6'),
    CARD_7('7'),
    CARD_8('8'),
    CARD_9('9'),
    CARD_10('T', "10"),
    CARD_JACK('J', "Jack"),
    CARD_QUEEN('Q', "Queen"),
    CARD_KING('K', "King"),
    CARD_ACE('A', "Ace");

    /**
     * The ID, as read from the input files.
     */
    private final char rankId;

    /**
     * Printable name.
     */
    private final String rankName;

    /**
     * Creates a card rank.
     * 
     * @param rankId
     *            the character representation of the rank.
     */
    private Rank(char rankId)
    {
        this(rankId, null);
    }

    /**
     * Creates a card rank.
     * 
     * @param rankId
     *            the character representation of the rank.
     * 
     * @param rankName
     *            the printable name. If null, use the rank ID for the printable name.
     */
    private Rank(char rankId, String rankName)
    {
        this.rankId = rankId;

        if (rankName == null)
        {
            this.rankName = Character.toString(rankId);
        }
        else
        {
            this.rankName = rankName;
        }
    }

    /**
     * Returns the ID for this rank.
     * 
     * @return the ID.
     */
    public char getId()
    {
        return this.rankId;
    }

    /**
     * Returns the printable name for this rank.
     * 
     * @return the printable name. This will be non-null.
     */
    public String getName()
    {
        return this.rankName;
    }

    /**
     * Compares 2 ranks and returns -1, 0 or 1 if the first rank is less than, equal, or greater than the second.
     * 
     * @param rank1
     *            the first rank
     * 
     * @param rank2
     *            the second rank
     * 
     * @return -1, 0 or 1
     */
    public static int compare(Rank rank1, Rank rank2)
    {
        if (rank1 == rank2)
        {
            return 0;
        }
        else if (rank1 == null)
        {
            return -1;
        }
        else if (rank2 == null)
        {
            return 1;
        }
        else
        {
            return Integer.compare(rank1.ordinal(), rank2.ordinal());
        }
    }
}
