package poker;

/**
 * The result for a single player in a single game.
 */
public enum Result
{
    WIN, LOSS, TIE, ERROR;
}
