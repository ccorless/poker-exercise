package poker;

import java.io.PrintStream;

/**
 * Useful constants.
 */
public class GameConstants
{
    /**
     * The number of cards in a hand.
     */
    public final static int NUMBER_CARDS = 5;

    /**
     * Command-line argument for low verbosity.
     */
    public final static String LOW_VERBOSITY_OPTION = "-v";

    /**
     * Command-line argument for high verbosity.
     */
    public final static String HIGH_VERBOSITY_OPTION = "-V";

    /**
     * Printable name for the 1st player.
     */
    public final static String PLAYER_1_NAME = "Player 1";

    /**
     * Printable name for the 2nd player.
     */
    public final static String PLAYER_2_NAME = "Player 2";

    /**
     * Regular expression for delimiting each line of text from the input.
     */
    public final static String DELIMITER_EXPRESSION = "\\W";

    /**
     * Output print stream.
     */
    public final static PrintStream OUTPUT_STREAM = System.out;
}
