package poker;

/**
 * Enumeration for output verbosity.  The more verbose, the more details.
 */
public enum Verbosity
{
    NONE, LOW, HIGH;
}
