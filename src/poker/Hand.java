package poker;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * A hand of 5 cards.
 */
public class Hand implements Comparable<Hand>
{
    /**
     * Cards in hand, ordered by rank, with highest rank first. This will be non-null.
     */
    private final List<Card> cards;

    /**
     * Card groups of the same rank. These are ordered with the highest rank first. This will be non-null.
     */
    private final List<Group> groups;

    /**
     * The type of hand. This will be non-null.
     */
    private final HandType handType;

    /**
     * Creates a hand consisting of 5 cards.
     * 
     * @param cards
     *            List of cards for the hand.
     */
    public Hand(List<Card> cards)
    {
        if (cards == null)
        {
            throw new PokerException("Hand has no cards");
        }
        else if (cards.size() != GameConstants.NUMBER_CARDS)
        {
            throw new PokerException("Hand has illegal number of cards.  There are {0} cards in the hand, it should contain exactly {1}",
                                     cards.size(),
                                     GameConstants.NUMBER_CARDS);
        }

        this.cards = cards.stream().filter(Objects::nonNull).collect(Collectors.toCollection(ArrayList::new));

        // Sort in reverse order.
        this.cards.sort(Comparator.reverseOrder());

        this.groups = computeGroups(this.cards);
        this.handType = computeHandType(this.cards, this.groups);
    }

    /**
     * Computes the groups.
     * 
     * @param cards
     *            the cards.
     * 
     * @return the groups. This will be non-empty.
     */
    private static List<Group> computeGroups(List<Card> cards)
    {
        List<Group> groupList = new ArrayList<>();

        // Map of cards based on rank. This is used to detect grouping (e.g. pairs).
        final Map<Rank, Integer> cardGroupMap = new HashMap<>();

        // Create list of groupings, with the largest groupings at the front.
        cards.stream().forEach(card -> {

            Rank rank = card.getRank();

            Integer count = cardGroupMap.get(rank);

            if (count == null)
            {
                count = 0;
            }

            cardGroupMap.put(rank, count + 1);
        });

        for (Map.Entry<Rank, Integer> entry : cardGroupMap.entrySet())
        {
            int size = entry.getValue();

            Group grp = new Group(entry.getKey(), size);
            groupList.add(grp);
        }

        // Sort in reverse order, i.e. with highest group first.
        groupList.sort(Comparator.reverseOrder());

        return groupList;
    }

    /**
     * Computes the hand type.
     * 
     * @param cards
     *            list of cards ordered with the lowest rank first.
     * 
     * @param groups
     *            list of groups, with highest group first.
     */
    private static HandType computeHandType(List<Card> cards, List<Group> groups)
    {
        // No groups, so look for flushes and straights.
        boolean sameSuit = isSameSuit(cards);
        boolean sequence = isSequence(cards);

        Card highCard = cards.get(0);

        // Compute the hand type.
        if (sequence)
        {
            if (sameSuit)
            {
                // Straight flush. If the last card is an Ace, this is a royal flush.
                if (highCard.getRank() == Rank.CARD_ACE)
                {
                    return HandType.ROYAL_FLUSH;
                }
                else
                {
                    return HandType.STRAIGHT_FLUSH;
                }
            }
            else
            {
                return HandType.STRAIGHT;
            }
        }
        else if (sameSuit)
        {
            return HandType.FLUSH;
        }

        // Look at the groupings. Note that a hand will always have at least 2 groups. And only the 2 highest groups can have sizes greater than 1.
        int hiSize = groups.get(0).getSize();
        int loSize = groups.get(1).getSize();

        if (hiSize == 4)
        {
            return HandType.FOUR_OF_A_KIND;
        }
        else if ((hiSize == 3) && (loSize == 2))
        {
            return HandType.FULL_HOUSE;
        }
        else if (hiSize == 3)
        {
            return HandType.THREE_OF_A_KIND;
        }
        else if ((hiSize == 2) && (loSize == 2))
        {
            return HandType.TWO_PAIRS;
        }
        else if (hiSize == 2)
        {
            return HandType.PAIR;
        }
        else
        {
            return HandType.HIGH_CARD;
        }
    }

    /**
     * Returns the type of the hand.
     * 
     * @return the hand type, this will be non-null.
     */
    public HandType getHandType()
    {
        return this.handType;
    }

    /**
     * Checks if the passed cards are a sequence.
     * 
     * @param cards
     *            the cards in the hand, ordered by rank, with highest rank first.
     */
    private static boolean isSequence(List<Card> cards)
    {
        Card firstCard = cards.get(0);
        int previous = firstCard.getRank().ordinal();

        for (int i = 1; i < cards.size(); i++)
        {
            Card nextCard = cards.get(i);
            int current = nextCard.getRank().ordinal();

            if (previous - current != 1)
            {
                // Not in sequence
                return false;
            }
            else
            {
                previous = current;
            }
        }

        // If we're here then all of the cards are in sequence.
        return true;
    }

    /**
     * Checks if all the passed cards have the same suit.
     * 
     * @param cards
     *            the cards in the hand.
     */
    private static boolean isSameSuit(List<Card> cards)
    {
        Card firstCard = cards.get(0);
        Suit suit = firstCard.getSuit();

        for (int i = 1; i < cards.size(); i++)
        {
            Card nextCard = cards.get(i);

            if (nextCard.getSuit() != suit)
            {
                // Different suit to the last
                return false;
            }
        }

        // If we're here then all of the cards have the same suit.
        return true;
    }

    /**
     * <p>
     * Compares this hand to the passed hand. Returns -1, 0 or 1 if this hand is less than, equivalent, or better than the passed hand.
     * </p>
     * <p>
     * The following criteria are used to compare the hands, in the following order:
     * <ol>
     * <li>The type of hand e.g. full house versus flush</li>
     * <li>If the type matches, compare the groups in each hand (including single card groups), starting with the highest ranked group</li>
     * </ol>
     * </p>
     * <p>
     * Note that even with these rules, you can have a tie. For example, both players could have a royal flush with different suits. In real poker you would
     * break the tied by comparing the suit of the highest ranked card. In this exercise however, the hands are considered equal i.e. they are tied.
     * </p>
     * 
     * @param hand
     *            the other card hand.
     * 
     * @return -1, 0 or 1 if this hand is less than, equivalent, or better than the other hand.
     */
    @Override
    public int compareTo(Hand hand)
    {
        if (hand == null)
        {
            return 1;
        }
        else if (hand == this)
        {
            return 0;
        }

        // Compare the hand type.
        int typeResult = this.handType.compareTo(hand.handType);

        if (typeResult != 0)
        {
            return typeResult;
        }

        // Now compare the groups.
        for (int i = 0; i < GameConstants.NUMBER_CARDS; i++)
        {
            Group thisGroup = null;

            if (this.groups.size() > i)
            {
                thisGroup = this.groups.get(i);
            }

            Group otherGroup = null;

            if (hand.groups.size() > i)
            {
                otherGroup = hand.groups.get(i);
            }

            if ((thisGroup == null) && (otherGroup == null))
            {
                // Can go no further.
                return 0;
            }
            else if (thisGroup == null)
            {
                // The other guy wins.
                return -1;
            }
            else if (otherGroup == null)
            {
                // We win.
                return 1;
            }
            else
            {
                int groupResult = thisGroup.compareTo(otherGroup);

                if (groupResult != 0)
                {
                    return groupResult;
                }
            }
        }

        // If we're here, we have a perfect match.
        return 0;
    }

    /**
     * Returns a printable representation of this object.
     */
    @Override
    public String toString()
    {
        StringBuilder text = new StringBuilder();
        text.append(this.handType.getName());
        text.append(": ");

        String cardText = this.cards.stream().filter(Objects::nonNull).map(p -> p.toString()).collect(Collectors.joining(", "));
        text.append(cardText);

        return text.toString();
    }

}
