package poker;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * A single game, which corresponds to a single line of input data.
 */
public class Game
{
    /**
     * Compiled expression for delimiting the input text.
     */
    private final static Pattern DELIMITER_PATTERN = Pattern.compile(GameConstants.DELIMITER_EXPRESSION);

    /**
     * Number of input cards per line.
     */
    private final static int NUMBER_CARDS_PER_LINE = 2 * GameConstants.NUMBER_CARDS;

    /**
     * The hand for player 1. This will be non-null.
     */
    private Hand hand1;

    /**
     * The hand for player 2. This will be non-null.
     */
    private Hand hand2;

    /**
     * The result, from player 1's viewpoint. This will be non-null.
     */
    private Result result = Result.ERROR;

    /**
     * Error message, or null if there are no errors.
     */
    private String errorMessage = null;

    /**
     * Creates a game for 2 players.
     * 
     * @param hand1
     *            hand for the 1st player. This must be non-null.
     * 
     * @param hand2
     *            hand for the 2nd player. This must be non-null.
     */
    private Game(Hand hand1, Hand hand2)
    {
        try
        {
            if (hand1 == null)
            {
                throw new PokerException("Empty hand for player ''{0}''", GameConstants.PLAYER_1_NAME);
            }

            this.hand1 = hand1;

            if (hand2 == null)
            {
                throw new PokerException("Empty hand for player ''{0}''", GameConstants.PLAYER_2_NAME);
            }

            this.hand2 = hand2;

            int compare = this.hand1.compareTo(this.hand2);

            if (compare == 0)
            {
                this.result = Result.TIE;
            }
            else if (compare < 0)
            {
                this.result = Result.LOSS;
            }
            else
            {
                this.result = Result.WIN;
            }
        }
        catch (Exception ex)
        {
            this.result = Result.ERROR;
            this.errorMessage = ex.getMessage();
        }
    }

    /**
     * Creates a game with an error.
     * 
     * @param errorMessage
     *            the error message.
     */
    private Game(String errorMessage)
    {
        this.errorMessage = errorMessage;
        this.hand1 = this.hand2 = null;
        this.result = Result.ERROR;
    }

    /**
     * Creates a game for 2 players based on a single line of input.
     * 
     * @param line
     *            the input line. This must be null.
     * 
     * @return the game. This will be null if the input line is empty.
     */
    public final static Game createGame(String line)
    {
        try
        {
            if (line == null)
            {
                return null;
            }

            String text = line.trim();

            if (text.isEmpty())
            {
                return null;
            }

            String[] components = DELIMITER_PATTERN.split(text);

            List<Card> cards = Arrays.stream(components)
                                     .filter(Objects::nonNull)
                                     .map(p -> p.trim())
                                     .filter(p -> !p.isEmpty())
                                     .map(p -> Card.createCard(p))
                                     .toList();

            if (cards.size() != NUMBER_CARDS_PER_LINE)
            {
                throw new PokerException("{0} cards in line, should be {1}", cards.size(), NUMBER_CARDS_PER_LINE);
            }

            List<Card> cardList1 = cards.subList(0, GameConstants.NUMBER_CARDS);
            List<Card> cardList2 = cards.subList(GameConstants.NUMBER_CARDS, NUMBER_CARDS_PER_LINE);

            return new Game(new Hand(cardList1), new Hand(cardList2));
        }
        catch (Exception ex)
        {
            return new Game(ex.getMessage());
        }
    }

    /**
     * Returns the hand for player 1.
     * 
     * @return the hand. This will be non-null unless the game has an error.
     */
    public Hand getHand1()
    {
        return this.hand1;
    }

    /**
     * Returns the hand for player 2.
     * 
     * @return the hand. This will be non-null unless the game has an error.
     */
    public Hand getHand2()
    {
        return this.hand2;
    }

    /**
     * Returns the result for player 1. Note that a loss for player 1 will be a win for player 2.
     * 
     * @return game result. This will be non-null.
     */
    public Result getResult()
    {
        return this.result;
    }

    /**
     * Returns the error message, if there were any errors. This method would typically only be called if {@link #getResult()} returned
     * {@link poker.Result#ERROR}.
     * 
     * @return the error message. This will be null if there were no errors.
     */
    public String getErrorMessage()
    {
        return this.errorMessage;
    }
}
