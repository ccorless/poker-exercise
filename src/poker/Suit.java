package poker;

/**
 * The suit of a card.
 */
public enum Suit
{
    DIAMOND('D', "Diamonds"), HEART('H', "Hearts"), SPADES('S', "Spaces"), CLUBS('C', "Clubs");

    /**
     * The ID, as read in input.
     */
    private final char suitId;

    /**
     * The suit name, for printing purposes.
     */
    private final String suitName;

    /**
     * Creates a card suit.
     * 
     * @param suitId
     *            the character representation of the suit.
     * 
     * @param suitName
     *            the printable name of the suit. This must be non-null.
     */
    private Suit(char suitId, String suitName)
    {
        if (suitName == null)
        {
            throw new PokerException("Null suit name");
        }

        this.suitId = suitId;
        this.suitName = suitName;
    }

    /**
     * Returns the ID for this suit.
     * 
     * @return the ID.
     */
    public char getId()
    {
        return this.suitId;
    }

    /**
     * Returns the name of the suit.
     * 
     * @return the suit name. This will be non-null.
     */
    public String getName()
    {
        return this.suitName;
    }
}
