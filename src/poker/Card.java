package poker;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Single card in a hand.
 */
public class Card implements Comparable<Card>
{
    /**
     * The rank of the card. This will be non-null.
     */
    private final Rank rank;

    /**
     * The suit of the card. This will be non-null.
     */
    private final Suit suit;

    /**
     * Lookup of ID versus suit.
     */
    private final static Map<Character, Suit> SUIT_MAP = Arrays.stream(Suit.values()).collect(Collectors.toMap(p -> p.getId(), p -> p));

    /**
     * Lookup of ID versus rank.
     */
    private final static Map<Character, Rank> RANK_MAP = Arrays.stream(Rank.values()).collect(Collectors.toMap(p -> p.getId(), p -> p));

    /**
     * Creates a card in a hand from the passed code.
     * 
     * @param code
     *            the code.
     * 
     * @return the card, or null if the code is invalid.
     */
    public static Card createCard(String code)
    {
        if (code == null)
        {
            throw new PokerException("Blank card");
        }
        else
        {
            Card card = new Card(code.trim());
            return card;
        }
    }

    /**
     * Creates a card in a hand.
     * 
     * @param text
     *            2-character representation of the card, where the first character is the rank, and the second character is the suit. This must be exactly 2
     *            characters. This will be non-null.
     */
    private Card(String text)
    {
        if (text == null)
        {
            throw new PokerException("Empty card code");
        }

        String code = text.trim();

        if (code.length() != 2)
        {
            throw new PokerException("Illegal card code ''{0}'' of length {1}", code, code.length());
        }

        char rankId = code.charAt(0);
        char suitId = code.charAt(1);

        this.rank = RANK_MAP.get(rankId);

        if (this.rank == null)
        {
            throw new PokerException("Illegal rank for card code ''{0}''", code);
        }

        this.suit = SUIT_MAP.get(suitId);

        if (this.suit == null)
        {
            throw new PokerException("Illegal suit for card code ''{0}''", code);
        }
    }

    /**
     * Returns the card rank.
     * 
     * @return the card rank.
     */
    public Rank getRank()
    {
        return this.rank;
    }

    /**
     * Returns the card suit.
     * 
     * @return the card suit.
     */
    public Suit getSuit()
    {
        return this.suit;
    }

    /**
     * Compares this card with another. Returns -1, 0, or 1 if this card is less than, equal to, or higher than the second. This will sort cards in order of
     * increasing rank.
     * 
     * @param other
     *            the other card.
     */
    @Override
    public int compareTo(Card other)
    {
        if (other == null)
        {
            return 1;
        }
        else
        {
            return Rank.compare(this.rank, other.rank);
        }
    }

    /**
     * Returns a string representation of this object.
     */
    @Override
    public String toString()
    {
        return this.rank.getName().concat(" of ").concat(this.suit.getName());
    }
}
